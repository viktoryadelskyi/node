const express = require("express");
const cors = require("cors");
const fs = require("fs");
const expressLogging = require("express-logging");
const logger = require("logops");
const app = express();
const port = 8080;

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(expressLogging(logger));

const filesFolder = "./files";

app.get("/", (req, res) => {
  res.send("Hey bro, it's base url that does nothing");
});

app.get("/api/files", (req, res) => {
  try {
    const response = {
      message: "Success",
      files: fs.readdirSync(filesFolder),
    };
    res.setHeader("Content-Type", "application/json");
    res.status(200).send(JSON.stringify(response));
  } catch (err) {
    res.status(500).send({ message: `Server error` });
  }
});

app.get("/api/files/:fileName", (req, res) => {
  try {
    if (fs.existsSync(`${filesFolder}/${req.params.fileName}`)) {
      const stats = fs.statSync(`${filesFolder}/${req.params.fileName}`);
      const fileInfo = {
        message: "Success",
        filename: req.params.fileName,
        content: fs
          .readFileSync(`${filesFolder}/${req.params.fileName}`)
          .toString(),
        extension: req.params.fileName.split(".").slice(-1).join(""),
        uploadedDate: stats.birthtime,
      };
      res.status(200).send(JSON.stringify(fileInfo));
    } else {
      res
        .status(400)
        .send({
          message: `No file with ${req.params.fileName} filename found`,
        });
    }
  } catch (err) {
    res.status(500).send({ message: `Server error` });
  }
});

app.post("/api/files", (req, res) => {
  try {
    if (!req.body.filename) {
      res.status(400).send({ message: '"filename" is empty. mandatory field' });
    } else if (!req.body.content) {
      res.status(400).send({ message: '"content" is empty. mandatory field' });
    } else if (
      !["log", "txt", "json", "yaml", "xml", "js"].includes(
        req.body.filename.split(".").slice(-1).join("")
      )
    ) {
      res
        .status(400)
        .send({
          message:
            "wrong extension. supported extensions: log, txt, json, yaml, xml, js",
        });
    } else if (fs.existsSync(`${filesFolder}/${req.body.filename}`)) {
      res
        .status(400)
        .send({ message: "filename with same name already exist" });
    } else {
      fs.writeFile(
        `${filesFolder}/${req.body.filename}`,
        req.body.content,
        { flag: "wx" },
        function (err) {
          res.status(200).send({ message: "File created successfully" });
          if (err) throw err;
        }
      );
    }
  } catch (err) {
    res.status(500).send({ message: `Server error` });
  }
});

app.listen(port, () => {
  console.log(`app listening on port ${port}`);
});
